import { User } from './../../models/user';
import { Message } from './../../models/message';
import { Component, ViewChild } from '@angular/core';
import { IonList } from '@ionic/angular';
 
@Component({
    selector: 'app-chat',
    templateUrl: 'chat.page.html',
    styleUrls: ['chat.page.scss']
})

export class ChatPage {

    @ViewChild(IonList, {static: false}) list: any;

    public messages: Message[] = [
        {
            text: 'Привет!',
            time: new Date().getTime(),
            user_id: 0
        },
        {
            text: 'Пока!',
            time: new Date().getTime() + 300,
            user_id: 1
        },
        // {
        //     text: 'Пока пока!',
        //     time: new Date().getTime() + 400,
        //     user_id: 1
        // },
        {
            text: 'Пока!',
            time: new Date().getTime() + 600,
            user_id: 2
        },
        {
            text: 'Добрый день!',
            time: new Date().getTime() + 900,
            user_id: 0
        }
    ];

    public users: User[] = [
        {
            id: 0,
            email: 'user0@test.ru',
            avatar: 'pig',
            name: 'UserPig',
            password: '123456'
        },
        {
            id: 1,
            email: 'user1@test.ru',
            avatar: 'blue',
            name: 'BlueBird',
            password: '123456'
        },
        {
            id: 2,
            email: 'user2@test.ru',
            avatar: 'yellow',
            name: 'YellowBird',
            password: '123456'
        }
    ];

    constructor() { }


    public scrollToBottom(): void {
        setTimeout(() => {
            const listElement = this.list.el;
            if (listElement) {
                listElement.scrollTo(0, 100000);
            }
        }, 0);
    }
}
