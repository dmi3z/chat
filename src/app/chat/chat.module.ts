import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChatPage } from './chat.page';
import { TimePipe } from 'src/pipes/time.pipe';
import { MessageComponent } from './message/message.component';

@NgModule({
    declarations: [
        ChatPage,
        TimePipe,
        MessageComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: ChatPage
            }
        ])
    ]
})

export class ChatPageModule { }
