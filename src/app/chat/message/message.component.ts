import { User } from './../../../models/user';
import { Message } from './../../../models/message';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-message',
    templateUrl: 'message.component.html',
    styleUrls: ['message.component.scss']
})

export class MessageComponent {

    @Input() message: Message;
    @Input() users: User[];

    constructor() { }

    public get avatar(): string {
        const user = this.users.find(u => u.id === this.message.user_id);
        return `assets/avatars/${user.avatar}.png`;
    }

    public get name(): string {
        return this.users.find(u => u.id === this.message.user_id).name;
    }

    public getIsRight(): boolean {
        // return this.currentUserId === userId;
        return false;
    }
}
