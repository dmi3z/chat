import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { emailValidator } from '../emailValidator';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-auth',
    templateUrl: 'auth.component.html',
    styleUrls: ['auth.component.scss']
})

export class AuthComponent implements OnInit {

    public email: FormControl;
    public password: FormControl;

    public userForm: FormGroup;

    constructor(private modalController: ModalController) { }

    ngOnInit() {
        this.createFormFields();
        this.createUserForm();
    }

    private createFormFields(): void {
        this.email = new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(30),
            emailValidator
        ]);

        this.password = new FormControl('', [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(20)
        ]);
    }

    private createUserForm(): void {
        this.userForm = new FormGroup({
            email: this.email,
            password: this.password
        });
    }

    public closeModal() {
        this.modalController.dismiss();
    }
}
