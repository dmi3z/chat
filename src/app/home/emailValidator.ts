import { FormControl } from '@angular/forms';
export function emailValidator(email: FormControl) {
    const value = email.value;

    const emailRegexp = /^([A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$)/i;

    const isValid = emailRegexp.test(value);

    if (isValid) {
        return null;
    } else {
        return {
            emailValidator: {
                valid: false
            }
        };
    }
}
