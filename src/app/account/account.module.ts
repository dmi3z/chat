import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { AccountPage } from './account.page';
import { ReactiveFormsModule } from '@angular/forms';
import { AvatarComponent } from './avatar/avatar.component';

@NgModule({
    declarations: [
        AccountPage,
        AvatarComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: AccountPage
            }
        ])
    ]
})

export class AccountPageModule { }
