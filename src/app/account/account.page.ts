import { User } from './../../models/user';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AvatarComponent } from './avatar/avatar.component';
import { Location } from '@angular/common';

@Component({
    selector: 'app-account-page',
    templateUrl: 'account.page.html',
    styleUrls: ['account.page.scss']
})

export class AccountPage implements OnInit {

    public name: FormControl;
    public password: FormControl;

    public userForm: FormGroup;

    private user: User = {
        id: 0,
        email: 'user0@test.ru',
        avatar: 'pig',
        name: 'UserPig',
        password: '123456'
    };

    public avatars: string[] = ['black', 'blue', 'pig_sad', 'pig', 'red_happy', 'red', 'yellow'];

    constructor(private location: Location) { }

    ngOnInit() {
        AvatarComponent.selectedAvatar = this.user.avatar;
        this.createFormFields();
        this.createUserForm();
    }

    private createFormFields(): void {
        this.name = new FormControl(this.user.name, [Validators.required, Validators.maxLength(10)]); // TODO: async validator
        this.password = new FormControl(this.user.password, [Validators.required, Validators.minLength(6), Validators.maxLength(20)]);
    }

    private createUserForm(): void {
        this.userForm = new FormGroup({
            name: this.name,
            password: this.password
        });
    }

    public onAvatarChange(avatar: string): void {
        this.user.avatar = avatar;
    }

    public cancel(): void {
        this.location.back();
    }
}
