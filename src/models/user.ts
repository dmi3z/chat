export interface User {
    email: string;
    name: string;
    id: number;
    avatar: string;
    password: string;
}
